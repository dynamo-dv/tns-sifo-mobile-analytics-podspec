Pod::Spec.new do |s|
  s.name                = 'TSMobileAnalytics'
  s.version             = '3.2.2'

  s.summary             = 'TSMobileAnalytics'
  s.homepage            = 'http://tns-sifo.se'

  s.license             = { :type => 'Commercial', :file => 'LICENSE' }
  s.author              = { 'TNS SIFO' => 'info@tns-sifo.se' }
  s.source              = { :git => 'https://bitbucket.org/dynamo-dv/tns-sifo-mobile-analytics-pod.git', :tag => s.version }

  s.preserve_paths      = 'Documentation/**/*', 'Samples/**/*'

  s.platform            = :ios, '9.0'
  s.vendored_frameworks = 'TSMobileAnalytics.framework'

  s.frameworks          = 'Foundation', 'UIKit', 'Security'
  s.module_name         = 'TSMobileAnalytics'
end
